# LDAP

Es un conjunto de protocolos abiertos usados para acceder información guardada centralmente a través de la red.

### Cambiar nombre de host
Primero de cambió el nombre del host, mediante el comando
```shell
hostname LDAP
```
Además de cambiar el nombre del host en los siguientes archivos <br>
En `/etc/hosts` se modificó la línea 8 para que quede

    127.0.1.1 LDAP

En `/etc/hostname` se modificó para que solo quedara

    LDAP

### Configurar zona horaria
Posteriormente se cambió la zona horaria del sistema, primero se instala dbus para evitar errores mediante el comando
```shell
apt install dbus
```
Y luego procede a cambiar la zona con los siguientes comandos
```shell
timedatectl
timedatectl list-timezones
timedatectls set-timezone America/Mexico_City
```
**Fuente:** https://nksistemas.com/cambiar-el-timezone-con-timedatectl-en-linux/

## Prerrequisitos SSL
Se necesita crear un grupo SSL-CERT, entonces ingresamos el siguiente comando 
 
    # groupadd --gid 107 --system ssl-cert

### Creación de certificados
Esta creación de dicho certificado permitirá mantener nuestra información cifrada y segura en los sitios web <br>
Se genera la llave privada del servidor con el comando
```shell
openssl genrsa -out server.key 2048
```
Usando la llave generada se genera el archivo csr
```shell
openssl req -new -key server.key -out server.csr
```

Con los siguientes datos

    Country Name (2 letter code) [MX]:DF <br>
    State or Province Name (full name) []:CDMX <br>
    Locality Name (eg, city) []:DF <br>
    Organization Name (eg, company) []:UNAM <br>
    Organizational Unit Name (eg, section) []:UNAM-CERT <br>
    Common Name (eg, YOUR name) []:directory.becarios.unam.mx <br>
    Email Address []:rgt_jman@hotmail.com <br>

Dicha petición (csr) se certificó mediante LetsEncrypt usando la pagina

    https://zerossl.com/free-ssl/#crt
    
La cual nos generó un archivo (el cual se subió al servidor mediante scp y se movió a `/var/www/html/.well-known/acme_challenge`) <br>
Obteniendo como resultado una llave publica y un certificado crt, los cuales se instalaron mediante los comandos
```shell
install -D -o root -g ssl-cert -m 644 server.crt /etc/ssl/certs/server.crt
install -D -o root -g ssl-cert -m 640 server.key /etc/ssl/private/server.key
```
Y se cambiaron los permisos y propietarios, usando los comandos
```shell
chmod 0644 /etc/ssl/private/server.key
chown root:ssl-cert /etc/ssl/certs/server.crt
chmod 0640 /etc/ssl/private/server.crt
chown root:ssl-cert /etc/ssl/private/server.key
```

**Instalación y configuración del LDAP**
-------------------------------------

* Configuración del gestor de paquetes 

    `dpkg-reconfigure debconf`

Con las siguientes configuraciones: 
 
        Interface to use: Dialog 
        Ignore questions with a priority less than: Low

En caso de que se haya cometido un error o se requiera volver a hacer la configuración ejecutar este comando 


* **Instalación del OPENLDAP**

    `apt-get install slapd`

Aparecerá varias pantallas y se debe ingresar las siguientes  configuraciones
    
    Omit OpenLDAP server configuration?   No
    DNS domain name directory.becarios.tonejito.info
    Organization name UNAM
    Administrator password hola123,
    Database backend to use MDB
    Do you want the database to be removed when slapd is purged? No
    Allow LDAPv2 protocol? No
    Should man and mandb be installed 'setuid man'? No

Posteriormente necesitamos agregar el usuario OPENLDAP 

    # adduser openldap ssl-cert 

Y modificar los permisos del directorio openssl 
 
    # chown root:ssl-cert /etc/ssl/private 
    # chmod g+x /etc/ssl/private 
    
Es importante recordar el **DNS domain name** porque se utilizará y se aplicará a dicho dominio todo lo que se haga posteriormente 

* **Configurando el certificado SSL para SLAPD**

    Se apagará el servicio para hacer las configuraciones 

    `# /etc/init.d/slapd stop` 
    
    Lo que siguiente que haremos es habilitar LDAPS en sus interfaces editando este archivo **/etc/dafault/slapd** y cambiar esta linea(Es la que representa al loopback)
    
        SLAPD_SERVICES="ldaps:/// ldap:/// ldapi:///"
    
    Se dejo abierta para que los servicios como mail y el servidor NFS no tuvieran problemas al conectar con LDAP

    Editar el **/etc/ldap/slapd.d/cn\=config.ldif** y buscar la linea **"olcToolThreads: 1"**, despues agregar estas lineas 
    
        # olcTLSCertificateFile: /etc/ssl/certs/server.crt
        # olcTLSCertificateKeyFile: /etc/ssl/private/server.key 

Además se debe borrar la segunda línea

    #CRC 323

Se edita el archivo `/etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb.ldif`
Eliminando tambien la segunda línea
    
    #CRC 323

Y en la línea donde dice olcRootPW se ellimina uno de los ":" y se coloca la contraseña del admin, quedando del siguiente modo

    olcRootPW: ********
    
Lo siguiente que debemos hacer es indizar la base de datos y reparar permisos, al ingresa el primer comando marcará un error pero es totalmente normal 
    
        # slapindex -F /etc/lapd/slapd.d 
        # chown -R openldap:openldap /var/lib/ldap 
    
    Se iniciará el servicio 
    
        # /etc/init.d/slapd start 

### Apache
Se instala apache con el comando
```shell
apt install apache2
```
Se actica el ssl y la pagina por defecto con los comandos
```shell
sudo a2enmod ssl
sudo a2ensite default-ssl
```
Se reinicia apache para que se apliquen los cambios con los comandos
```shell
/etc/init.d/apache2 reload
/etc/init.d/apache2 restart
```
Se edita el archivo `/etc/apache2/sites-enabled/default-ssl.conf`, modificando o descomentando las siguientes líneas
```
<VirtualHost _default_:443>
...
ServerName directory.becarios.tonejito.info:443
SSLEngine on
SSLCertificateFile  /etc/ssl/certs/server.crt
SSLCertificateKeyFile /etc/ssl/private/server.key
...
</VirtualHost>
```
Se vuelve a reiniciar apache para que se apliquen los cambios con el comando
```shell
/etc/init.d/apache2 reload
/etc/init.d/apache2 restart
```
Se verifica que se haya configurado correctamente con el siguiente comando
```shell
openssl s_client -connect directory.becarios.tonejito.info:443
```
Para hacer que se redirija automáticamente de http a https se modifica el archivo `/etc/apache2/sites-enabled/000-default.conf`  añadiendo las siguientes líneas
```
<VirtualHost *:80>
...
RewriteEngine on
rewriteCond %{HTTPS} !^on$ [NC]
RewriteRule . https://%{HTTP_HOST}%{REQUEST_URI} [l]
...
</VirtualHost>
```
Se modifica el archivo `/etc/apache2/conf-available/security.conf` para brindar mayor seguridad del sistema, añadiendo las siguientes líneas
```
<Directory /var/www/html/.well-known>
   AllowOverride None
   Order Deny,Allow
   Deny from all
</Directory>
ServerTokens ProductOnly
ServerSignature Off
TraceEnable Off
```
Además, se modificó la página principal del sitio en el archivo `/var/www/html/index.html` con el siguiente código
```
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
Bienvenidos!<br>Somos el equipo de LDAP<br>Con dominio: directory.becarios.tonejito.info<br>IP: 18.224.160.100<br><img src="./img.jpg">
  </body>
</html>
```
Se subió la imagen mediante el comando scp <br>
**Fuente:** https://www.digitalocean.com/community/tutorials/how-to-create-a-ssl-certificate-on-apache-for-debian-8

        
**Configurar la autenticación por ldap** 
---------------------------------------

* **Instalar el paquete de soporte de LDAP para PAM** 

    # apt-get install libpam-ldap

Aparecerán varias pantallas y se deben guardar con las siguientes configuracion 

    LDAP server URI: ldap://127.0.0.1:389/
    LDAP server search base: dc=admin,dc=directory,dc=becarios,dc=tonejito,dc=info 
    LDAP database user: (dejar en blanco)
    Use StartTLS? No
    Name services to configure:
        [*] aliases
        [*] group
            [*] passwd
            [*] shadow
    PAM profiles to enable:
            [*] Unix authentication
            [*] LDAP Authentication

En la configuración ** LDAP server URI: ldap://127.0.0.1:389** representa el loopback por el puerto 389, que es el indica el servicio de LDAP. Por otra parte en la configuración **LDAP Server search base** se debe agregar el dc=admin y agregar lo que se menciono anteriormente en la configuración del **DNS domain name** que en este caso es "dc=directory,dc=becarios,dc=tonejito,dc=info"

* Configurar apache httpd con ssl-> Juan 

Se tiene que editar el archivo **/etc/apache2/sites-enabled/default-ssl.** Esto se hace para que se guarde y configure el certificado que anteriormente se generó 

Se tienen que cambiar estas lineas 

    # SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem 
    # SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key 

Por estas 

    # SSLCertificateFile /etc/ssl/certs/server.crt 
    # SSLCertificateKeyFile /etc/ssl/private/server.key
    
Reiniciar el servicio 

    # etc/init.d/apache2 restart 

Instalar y configurar la interfaz administrativa ldap-account-manager

    # apt-get install ldap-account-manager 
    
Con las siguientes configuraciones 

    Web server configuration
    [*] apache2
    Alias name: lam
    Restart webservers now? Yes

* **Redireccionar las peticiones a HTTPS**

* **Configuración de la interfaz administrativa** 


Configuración de la interfaz administrativa 
Al ingresar a nuestro dominio, en este caso directory.becarios.tonejito.info, se verá algo así 


![texto][img1]

[img1]: Imagenes/1.png


Posteriormente para ingresar a LDAP se debe agregar esto al link **https://directory.becarios.tonejito.info/lam** que se muestra con la pantalla siguiente 


![texto][img2]

[img2]: Imagenes/8.png


Como se puede observar en la pantantalla, en la parte superior izquierda (señalado), dar click, y gracias a esto se harán las configuraciones siguientes.


![texto][img3]

[img3]: Imagenes/9.png

En **tree sufix** poner lo que se ha puesto en **Domain name** (Lo que hizo desde la primera configuración) 

![texto][img4]

[img4]: Imagenes/2.png

![texto][img5]

[img5]: Imagenes/3.png



![texto][img6]

[img6]: Imagenes/4.png


Se deben de guardar las configuraciones y después en la página principal, iniciar sesión. Automáticamente cuando se hayan hecho las configuraciones pasadas se deben de poner en automatico el user que hará el login 
Si se tiene problemas al iniciar sesión es importante volver a revisar las configuraciones que se han hecho anteriormente, uno de estos problemas puede los nombre de dominio que se utilizan regularmente. 

Una vez iniciado sesión se deben de agregar a los usuarios y grupos que son necesarios, en este caso se crearon a todos los alumnos de becarios en el grupo "becarios" 


![texto][img7]

[img7]: Imagenes/5.png


Cuando se agrega a los usuarios, lo importante que se debe hacer es hacer ver donde se guardarán en este caso se guardó en /srv/home 




### Creación de usuario delegado
1. Accedemos a la dirección `directory.becarios.tonejito.info/lam`
2. Ingresamos la contraseña "hola123,"
3. Seleccionamos la opción tree
4. Realizamos una copia del usuario admin en la misma ubicación del árbol
5. Le asignamos el nombre de usuario "delegate"


![texto][imgDelegate]

[imgDelegate]: Imagenes/delegate.png


### Dar permisos a usuario delegado
Modificamos el archivo `/etc/ldap/slap.d/cn=config/olcDatabase={1}mdb.ldif` <br>
Y añadimos la siguiente línea
```
olcAccess: {3}to attrs=userPassword by dn="cn=delegate,dc=directory,dc=becarios,dc=tonejito,dc=info" write by dn="cn=delegate,dc=directory,dc=becarios,dc=tonejito,dc=info" read
```
Del cual su sintaxis básica es la siguiente
```
olcAcces: {_Número_de_acceso_}to _Objetos_a_acceder_ by _usuario_ _permiso_
```    

### LDAP Toolbox self service 
Editamos el archivo `/etc/apt/sources.list` añadiendo el siguiente repositorio
```
deb http://ltb-project.org/debian/jessie jessie main
```
Importamos el repositorio con el comando
```shell
wget -O - http://ltb-project.org/wiki/lib/RPM-GPG-KEY-LTB-project | sudo apt-key add -
```
Actualizamos
```shell
apt-get update
```
E instalamos el paquete
```shell
apt-get install self-service-password
```

Copiamos la carpeta instalada a la carpeta raíz del servidor apache mediante el comando
```shell
cp -r /usr/share/self-service-password /var/www/html/
```
Editamos el archivo `/etc/apache2/sites-enabled/000-default.conf` añadiendo las siguientes líneas
```
<VirtualHost *:80>
    ... 
    <Directory /self-service-password>
        AllowOverride None
        <IfVersion >= 2.3>
            Require all granted
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Allow from all
        </IfVersion>
    </Directory>
 
    <Directory /self-service-password/scripts>
        AllowOverride None
        <IfVersion >= 2.3>
            Require all denied
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Deny from all
        </IfVersion>
    </Directory>
 
    LogLevel warn
    ErrorLog /var/log/apache2/ssp_error.log
    CustomLog /var/log/apache2/ssp_access.log combined
</VirtualHost>
```
Y habilitamos el servicio con el comando
```shell
a2ensite self-service-password
```
Antes de modificar las características de self-service-password, instalamos la librería de pam para administrar políticas de contraseñas con el comando
```
apt install libpam-cracklib
```
Para configurar la página principal de self-service-password se modifica el archivo `/var/www/html/self-service-password/conf/config.inc.php`, borrando todo el contenido y añadiendo las siguientes líneas
```
<?php
// Override config.inc.php parameters below
 
?>
```
Y añadimos las siguientes líneas

### Configuraciones generales
Definir el idioma en ingles
```
$lang = "en";
```
Muestra el menú superior
```
$show_menu = true;
```
Muestra ayuda al usuario
```
$show_help = true;
```
Genera la bitácora de errores en archivo
```
$debug = true;
```
Muestra imagen de fondo
```
$background_image = "images/unsplash-space.jpeg";
```
Excluye caracteres especiales para evitar LDAP injection
```
$login_forbidden_chars = "*()&|";
```
Evita el uso de reCAPTCHA   
```
$use_recaptcha = false; 
```
Selecciona la opción predefinida para la página principal
```
$default_action = "change";
```
No usar contraseñas en formado Unicode
```
$ad_mode = false;
```

### Conexión a LDAP
Hacemos la conexión LDAP mediante mecanismos IPC
```
$ldap_url = "ldapi:///";
```
Desactivamos TTLS
```
$ldap_starttls = false;
```
Definimos la autenticación mediante el usuario delegado
```
$ldap_binddn = "cn=delegate,dc=directory,dc=becarios,dc=tonejito,dc=info";
```
Definimos la contraseña para el usuario delegado
```
$ldap_bindpw = "hola123,";
```
Se define la base de búsqueda
```
$ldap_base = "dc=directory,dc=becarios,dc=tonejito,dc=info";
```
Se especifica el atributo para el inicio de sesión
```
$ldap_login_attribute = "uid";
```
Se específica la variable del id del usuario
```
$ldap_fullname_attribute = "cn";
```
Se especifica el filtro de búsqueda
```
$ldap_filter = "(&(objectClass=person)($ldap_login_attribute={login}))";
```
Se desactiva historial de contraseñas
```
$use_pwnedpasswords = false;
```
### Políticas de contraseñas
Aplicar hash antes de enviar a LDAP
```
$hash = "sha";
```
Definir tamaño mínimo de contraseña
```
$pwd_min_length = 6;
```
Definir número minino de minúsculas
```
$pwd_min_lower = 1;
```
Definir número mínimo de mayúsculas
```
$pwd_min_upper = 1;
```
Definir número mínimo de dígitos
```
$pwd_min_digit = 1;
```
Definir numero mínimo de caracteres especiales
```
$pwd_min_special = 1;
```
Definir cuales son los caracteres especiales
```
$pwd_special_chars = "^.,$%&";
```
No verificar si la contraseña ha sido comprometida (se verifica online)
```
$use_pwnedpasswords = false;
```
Evitar que se reúsen contraseñas anteriores
```
$pwd_no_reuse = true;
```
Para acceder al servicio de self service password, lo hacemos mediante la siguiente dirección web
```
directory.becarios.tonejito.info/self-service-password
```

**OPENVPN**
-----------

* **Instalacion**

Para hacer uso de la VPN primero necesitamos instalar el paquete de openvpn

	# apt install openvpn

* **Configuracion**

Para la configuracion, el equipo encargado de proveer el servicio nos proporsiono el archivo de configuracion. Este archivo se guardo en el directorio **/etc/openvpn**

Para lograr que la configuracion se realice de forma automatica al reiniciar el equipo se realizaron las siguientes configuraciones.

*	Copiar el contenido del arhivo de configuracion que nos proporcionaron a un archivo llamado **service.conf**

    `# cp /etc/openvpn/directory.ovpn /etc/openvpn/service.conf`

*	Descomentamos la linea ___AUTOSTART="all"___ del archivo **/etc/default/openvpn** 

    `# nano /etc/default/openvpn`

*	Lo siguiente es ejecutar un comando para que la moficacion del archivo anterior sea tomada en cuenta

    `# systemctl daemon-reload`


**IPTABLES**
---------


Se ingresaron las siguientes lineas, se debe verificar y hacer una lista de todos lo puertos e IPs que se ocuparán dentro de este. En este caso se ocuparon las siguientes reglas: 

            
            -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT
            -A INPUT -p udp -m udp --dport 53 -j ACCEPT
            -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
            -A INPUT -p tcp -m multiport --dports 389,636 -j ACCEPT
            -A INPUT -s 54.244.81.249/32 -j ACCEPT
            -A INPUT -s 10.0.2.9/32 -j ACCEPT
            -A INPUT -s 18.220.184.122/32 -j ACCEPT
            -A INPUT -s 18.224.238.25/32 -j ACCEPT
            -A INPUT -s 18.217.216.172/32 -j ACCEPT
            -A INPUT -s 18.216.73.191/32 -j ACCEPT
            -A INPUT -s 132.247.0.0/16 -j ACCEPT
            -A INPUT -s 132.248.0.0/16 -j ACCEPT
            -A INPUT -s 10.8.0.0/24 -j ACCEPT
            -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
            -A INPUT -p icmp -j ACCEPT
            -A INPUT -s 10.8.0.0/24 -j ACCEPT
            -A OUTPUT -o lo -j ACCEPT
            -A OUTPUT -p tcp -m state --state RELATED,ESTABLISHED -m tcp --sport 53 -j ACCEPT
            -A OUTPUT -p udp -m state --state RELATED,ESTABLISHED -m udp --sport 53 -j ACCEPT
            -A OUTPUT -p tcp -m tcp --sport 22 -j ACCEPT
            -A OUTPUT -p tcp -m multiport --sports 389,636 -j ACCEPT
            -A OUTPUT -d 54.244.81.249/32 -j ACCEPT
            -A OUTPUT -d 10.0.2.9/32 -j ACCEPT
            -A OUTPUT -d 18.220.184.122/32 -j ACCEPT
            -A OUTPUT -d 18.224.238.25/32 -j ACCEPT
            -A OUTPUT -d 18.217.216.172/32 -j ACCEPT
            -A OUTPUT -d 18.216.73.191/32 -j ACCEPT
            -A OUTPUT -d 132.247.0.0/16 -j ACCEPT
            -A OUTPUT -d 132.248.0.0/16 -j ACCEPT
            -A OUTPUT -d 10.8.0.0/24 -j ACCEPT
            -A OUTPUT -p tcp -m tcp --sport 443 -j ACCEPT
            -A OUTPUT -p icmp -j ACCEPT
            -A OUTPUT -d 10.8.0.0/24 -j ACCEPT

Después se redireccionó 
```
iptables-save > rules.v4
```
En este paso debemos verificar verificar que las iptables esten en el archivo `/etc/iptables/rules.v4`
Una vez verificado esto debemos descargar el paquete 
```
apt install iptables persistent
```
El siguiente comando a ingresar es 
```
Service netfilter-persistent save iptables.v4
```
con este comando se indica que las reglas serán persistentes y que al reiniciar la computadora se quedarán de forma permamente. 
Es importante recordar que estas reglas tienen un orden, y que si no se ingresan de forma correcta se puede bloquear la máquina. 

    
    
